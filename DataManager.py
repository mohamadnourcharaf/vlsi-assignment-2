import random

import scipy.io
import networkx as nx

import Node
import HyperEdge


def getHypergraphFromMatlabFile(matlabFilePath,minRandomWeight,maxRandomWeight):

    mat = scipy.io.loadmat(matlabFilePath)

    A = mat["Problem"][0][0][1].toarray()

    G = nx.from_numpy_matrix(A)

    gNodes = list(G.nodes())
    gEdges = list(G.edges())

    nodes = []

    for i in range(0,len(gNodes)):
        node = Node.Node()
        node.id = i
        node.weight = random.randrange(maxRandomWeight) + minRandomWeight
        nodes.append(node)

    for i in range(0,len(gEdges)):
        hyperEdge = HyperEdge.HyperEdge()
        for n in gEdges[i]:
            hyperEdge.nodes.append(nodes[n])
            nodes[n].hyperEdges.append(hyperEdge)

    return nodes


def generateRandomHyperGraph(randomHyperGraphNodeSize,randomHyperGraphHyperEdgeSize,minRandomWeight,maxRandomWeight,randomProbability):

    nodes = []

    for i in range(0,randomHyperGraphNodeSize):
        node = Node.Node()
        node.id = i + 1
        node.weight = random.randrange(maxRandomWeight) + minRandomWeight
        nodes.append(node)

    for i in range(0,randomHyperGraphHyperEdgeSize):

        hyperEdge = HyperEdge.HyperEdge()
        for node in nodes:
            if random.uniform(0,1) < randomProbability:
                hyperEdge.nodes.append(node)
                node.hyperEdges.append(hyperEdge)

    return nodes


def getHyperGraphFromLecture():
    nodes = []

    node1 = Node.Node()
    node1.id = 1
    node1.weight = 2
    node1.partition = "A"
    nodes.append(node1)

    node2 = Node.Node()
    node2.id = 2
    node2.weight = 4
    node2.partition = "A"
    nodes.append(node2)

    node3 = Node.Node()
    node3.id = 3
    node3.weight = 1
    node3.partition = "B"
    nodes.append(node3)

    node4 = Node.Node()
    node4.id = 4
    node4.weight = 4
    node4.partition = "B"
    nodes.append(node4)

    node5 = Node.Node()
    node5.id = 5
    node5.weight = 5
    node5.partition = "B"
    nodes.append(node5)

    hyperEdge1 = HyperEdge.HyperEdge()
    hyperEdge1.nodes.append(node1)
    hyperEdge1.nodes.append(node2)

    hyperEdge2 = HyperEdge.HyperEdge()
    hyperEdge2.nodes.append(node1)
    hyperEdge2.nodes.append(node2)
    hyperEdge2.nodes.append(node3)

    hyperEdge3 = HyperEdge.HyperEdge()
    hyperEdge3.nodes.append(node1)
    hyperEdge3.nodes.append(node4)

    hyperEdge4 = HyperEdge.HyperEdge()
    hyperEdge4.nodes.append(node1)
    hyperEdge4.nodes.append(node5)

    hyperEdge5 = HyperEdge.HyperEdge()
    hyperEdge5.nodes.append(node3)
    hyperEdge5.nodes.append(node4)

    node1.hyperEdges.append(hyperEdge1)
    node1.hyperEdges.append(hyperEdge2)
    node1.hyperEdges.append(hyperEdge3)
    node1.hyperEdges.append(hyperEdge4)

    node2.hyperEdges.append(hyperEdge1)
    node2.hyperEdges.append(hyperEdge2)

    node3.hyperEdges.append(hyperEdge2)
    node3.hyperEdges.append(hyperEdge5)

    node4.hyperEdges.append(hyperEdge3)
    node4.hyperEdges.append(hyperEdge5)

    node5.hyperEdges.append(hyperEdge4)

    return nodes