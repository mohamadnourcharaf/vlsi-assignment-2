class Node:

    def __init__(self):
        self.id = None
        self.weight = None
        self.hyperEdges = []
        self.partition = None
        self.gain = None
        self.isFixed = None
        self.positiveGain = None
        self.areaADistanceFromBalanceCriteriaCenter = None