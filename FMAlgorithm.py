import random


def partitionWithFM(nodes,maxNumberOfPasses,partitionAlreadyInitialized):

    # Initialize Random Partition
    if not partitionAlreadyInitialized:
        initializeRandomPartition(nodes)

    # Print Initial Partitions
    printPassPartitions(nodes,"Initial Partitions",None)

    # Get Total Cell Area
    totalCellArea = getTotalCellArea(nodes)

    # Get Max Cell Area
    maxCellArea = getMaxCellArea(nodes)

    passNumber = 0
    while 1:

        # Ratio Factor
        areaA = getAreaA(nodes)
        areaB = getAreaB(nodes)
        ratioFactor = areaA / (areaA + areaB)

        # LowerBound
        lowerBound = (ratioFactor * totalCellArea) - maxCellArea
        upperBound = (ratioFactor * totalCellArea) + maxCellArea

        # Move Nodes
        movedNodes = []
        while 1:
            notFixedNodes = []
            for node in nodes:
                if not node.isFixed:
                    notFixedNodes.append(node)

            if len(notFixedNodes) == 0:
                break

            # Compute Gain of each cell
            for node in notFixedNodes:
                node.gain = getGain(node)

            # Choose base cell (node with max gain) without violating criterion
            baseCell = chooseBaseCellWithBalanceCriterion(nodes,notFixedNodes, lowerBound, upperBound)

            # Stop moving nodes no base cell is selected
            if baseCell is None:
                baseCellIsNone = True
                break

            # Move Base Cell
            moveCell(baseCell)

            # Fix Base Cell
            baseCell.isFixed = True

            # Save areaADistanceFromBalanceCriteriaBounds
            baseCell.areaADistanceFromBalanceCriteriaCenter = abs(getAreaA(nodes) - ((lowerBound + upperBound)/2))

            # Save to moved nodes
            movedNodes.append(baseCell)

        # Find maximum positive gain to determine best move sequence
        positiveGain = 0
        for node in movedNodes:
            positiveGain += node.gain
            node.positiveGain = positiveGain

        maxPositiveGainNode = max(movedNodes, key=lambda item: item.positiveGain)

        # Check if Max Positive Gain is <= 0, if it is then END
        if maxPositiveGainNode.positiveGain <= 0:
            # Revert all moved nodes and END
            for node in movedNodes:
                moveCell(node)
            print("END: Max Positive Gain is negative or zero\n")
            break

        maxPositiveGainNodes = []
        for node in movedNodes:
            if node.positiveGain == maxPositiveGainNode.positiveGain:
                maxPositiveGainNodes.append(node)

        # Pick Node with Max Positive Gain and Best Balance Ratio
        bestNode = min(maxPositiveGainNodes, key=lambda item: item.areaADistanceFromBalanceCriteriaCenter)

        # Fix move sequence up to the point of maximum positive gain (This is achieved by reversing move of remaining nodes)
        bestNodeIndex = nodes.index(bestNode)
        for i in range(bestNodeIndex + 1, len(movedNodes)):
            node = movedNodes[i]
            if node.isFixed:
                moveCell(movedNodes[i])

        # Reset Nodes
        for node in nodes:
            node.isFixed = False

        # Print Pass Partitions
        print("Pass " + str(passNumber + 1))
        printPassPartitions(nodes,"",bestNode.positiveGain)

        passNumber = passNumber + 1

        # Termination Criteria if reached max number of passes
        if passNumber == maxNumberOfPasses:
            print("END: Reached Max Number of Passes\n")
            break

    # Print Final Result
    printPassPartitions(nodes,"Final Partitions",None)


def printPassPartitions(nodes,message,maxPositiveGain):
    if message:
        print(message)
    if maxPositiveGain:
        print("Max Positive Gain: " + str(maxPositiveGain))
    print("Nodes in Partition A: ", end="")
    for node in nodes:
        if node.partition == "A":
            print(node.id, end=" ")
    print("")
    print("Nodes in Partition B: ", end="")
    for node in nodes:
        if node.partition == "B":
            print(node.id, end=" ")
    print("")
    print("")


def chooseBaseCellWithBalanceCriterion(nodes,notFixedNodes, lowerBound, upperBound):
    notFixedNodes.sort(key=lambda item: item.gain, reverse=True)
    baseCell = None
    for node in notFixedNodes:
        moveCell(node)  # Temporary Move
        areaA = getAreaA(nodes)
        if lowerBound <= areaA <= upperBound:
            baseCell = node
            moveCell(node)  # Revert Temporary Move
            break
        else:
            moveCell(node)  # Revert Temporary Move

    return baseCell


def moveCell(node):
    node.partition = "B" if node.partition == "A" else "A"


def getGain(node):
    return getMovingForce(node) - getRetentionForce(node)


def getRetentionForce(node):
    retentionForce = 0
    for hyperEdge in node.hyperEdges:
        hyperEdgeNotCut = True
        for n in hyperEdge.nodes:
            if n == node:
                continue
            if n.partition != node.partition:
                hyperEdgeNotCut = False
                break
        if hyperEdgeNotCut:
            retentionForce += 1

    return retentionForce


def getMovingForce(node):
    movingForce = 0
    for hyperEdge in node.hyperEdges:
        hyperEdgeCut = True
        for n in hyperEdge.nodes:
            if n == node:
                continue
            if n.partition == node.partition:
                hyperEdgeCut = False
                break
        if hyperEdgeCut:
            movingForce += 1

    return movingForce


def getAreaB(nodes):
    areaB = 0
    for node in nodes:
        if node.partition == "B":
            areaB += node.weight
    return areaB


def getAreaA(nodes):
    areaA = 0
    for node in nodes:
        if node.partition == "A":
            areaA += node.weight
    return areaA


def getMaxCellArea(nodes):
    maxNode = max(nodes, key=lambda item: item.weight)
    return maxNode.weight


def getTotalCellArea(nodes):
    sum = 0
    for node in nodes:
        sum += node.weight
    return sum


def initializeRandomPartition(nodes):
    for node in nodes:
        node.partition = "A" if random.uniform(0, 1) < 0.5 else "B"
