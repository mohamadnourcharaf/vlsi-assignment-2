import Parameters
import DataManager
import FMAlgorithm


def start():

    # Generate Random HyperGraph
    randomNodes = DataManager.generateRandomHyperGraph(Parameters.randomHyperGraphNodeSize,
                                                       Parameters.randomHyperGraphHyperEdgeSize,
                                                       Parameters.minRandomWeight,
                                                       Parameters.maxRandomWeight,
                                                       Parameters.randomProbability)

    # Partition with FM Algorithm
    print("****************************************************************************************************")
    print("Random Hyper Graph Example:\n")
    FMAlgorithm.partitionWithFM(randomNodes,Parameters.maxNumberOfPasses,False)

    # Get Lecture Example Nodes
    lectureNodes = DataManager.getHyperGraphFromLecture()

    # Partition with FM Algorithm
    print("****************************************************************************************************")
    print("Lecture Hyper Graph Example:\n")
    FMAlgorithm.partitionWithFM(lectureNodes,Parameters.maxNumberOfPasses,True)

    # Add20 Matlab HyperGraph
    add20Nodes = DataManager.getHypergraphFromMatlabFile(Parameters.add20MatlabFilePath,Parameters.minRandomWeight,Parameters.maxRandomWeight,)

    # Partition with FM Algorithm
    print("****************************************************************************************************")
    print("Add20 Hyper Graph Example:\n")
    FMAlgorithm.partitionWithFM(add20Nodes, Parameters.maxNumberOfPasses, False)


start()
