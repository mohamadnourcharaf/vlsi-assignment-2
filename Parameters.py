
add20MatlabFilePath = "add20.mat"

maxNumberOfPasses = 6

randomHyperGraphNodeSize = 5
randomHyperGraphHyperEdgeSize = 5
minRandomWeight = 1
maxRandomWeight = 5
randomProbability = 0.2